package mx.edu.itlapiedad.demo.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.itlapiedad.demo.dao.ClienteDAO;
import mx.edu.itlapiedad.demo.modelos.Cliente;

@Service
public class LogicaCliente implements ClienteService {

	@Autowired
	ClienteDAO repositorio;
	
	@Override
	public List<Cliente> consultar() {
		return repositorio.consultar();
	}

	@Override
	public Cliente buscar(int id) {
		return repositorio.buscar(id);
	}

	@Override
	public Cliente crear(Cliente cliente) {
		int id = repositorio.crear(cliente);
		cliente.setId(id);
		return cliente;
	}

	@Override
	public void modificar(Cliente cliente) {
		repositorio.modificar(cliente);
	}

	@Override
	public void borrar(int id) {
		repositorio.borrar(id);
	}

}
