package mx.edu.itlapiedad.demo.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import mx.edu.itlapiedad.demo.modelos.Cliente;

@Repository
public class ClienteJDBC implements ClienteDAO {

	@Autowired
	JdbcTemplate conexion; 
	
	String sql = "";
	
	@Override
	public List<Cliente> consultar() {
		sql = "SELECT * FROM clientes WHERE activo = 1";
		return conexion.query(sql, new ClienteRM());
	}

	@Override
	public Cliente buscar(int id) {
		sql = "SELECT * FROM clientes WHERE id = ? AND activo = 1";
		return conexion.queryForObject(sql, new ClienteRM(), id);
	}

	@Override
	public int crear(Cliente cliente) {
		SimpleJdbcInsert insert = new  SimpleJdbcInsert(conexion);
		List<String> columnas = new ArrayList<>();
		columnas.add("nombre");
		columnas.add("domicilio");
		columnas.add("rfc");
		insert.setTableName("clientes");
		insert.setColumnNames(columnas);	
		Map<String, Object> datos = new HashMap<>();
		datos.put("nombre", cliente.getNombre());
		datos.put("domicilio", cliente.getDomicilio());
		datos.put("rfc", cliente.getRfc());
		insert.setGeneratedKeyName("id");
		Number id = insert.executeAndReturnKey(datos);
		return id.intValue();
	}

	@Override
	public void modificar(Cliente cliente) {
		sql = "UPDATE clientes SET nombre = ?, domicilio = ?, rfc = ? WHERE id = ?";
		conexion.update(sql, cliente.getNombre(), cliente.getDomicilio(), cliente.getRfc(),
				cliente.getId());
	}

	@Override
	public void borrar(int id) {
		// DELETE FROM clientes WHERE id = ?
		sql = "UPDATE clientes SET activo = 0 WHERE id = ?";
		conexion.update(sql, id);
	}

}
